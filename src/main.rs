#[allow(warnings)]
// mod lib;
// mod lib_40;
#[allow(warnings)]

fn main() {
    // let data = "I like rust,do't ask me why?".as_bytes();
    // //key length must be equal to 16
    // let key = "alen_andryalen_a".as_bytes();
    // let enc = lib_40::encrypt(data.to_vec(), key.to_vec());
    // println!("{:?}", enc);
    // let dec = lib_40::decrypt(enc.to_vec(), key.to_vec());
    // println!("{:?}", lib_40::display_decrypt(dec));
    // run();
    arg_enc();
}
#[allow(warnings)]
fn arg_enc() {
    use idea_crypto::*;
    use useful_macro::*;
    let file = args!();
    let key = "this is password".as_bytes();
    encrypt_file(&file[0], key);
    decrypt_file(&file[0], key);
}
#[allow(warnings)]

fn run() {
    {
        //string
        use idea_crypto::*;
        let data = "This is plaintext".as_bytes();
        let key = "this is password".as_bytes(); //key length can be any length
        let enc = encrypt(data, key);
        let dec = decrypt(enc.clone(), &key);

        assert_eq!(
            enc,
            vec![
                vec![115, 247, 88, 166, 138, 225, 25, 243],
                vec![194, 236, 21, 196, 218, 159, 127, 117],
                vec![119, 7, 4, 154, 98, 218, 123, 31, 7]
            ]
        );
        assert_eq!(display_decrypt(dec), format!("This is plaintext"));

        //test
        let data1 = "This is another plaintext".as_bytes();
        let key1 = "key length can be any length key length can be any length ".as_bytes();
        let enc1 = encrypt(data1, key1);
        let dec1 = decrypt(enc1, key1);
        assert_eq!(display_decrypt(dec1), format!("This is another plaintext"));
        encrypt_file("./0.txt", key1);
        decrypt_file("./0.txt", key1);
    }
}
